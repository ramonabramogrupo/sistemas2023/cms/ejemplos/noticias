<?php

use app\models\Noticia;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // numero de orden
            // ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'titulo',
            //'contenido:ntext',
            'autor',
            'fechaPublicacion',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'headerOptions' => ['width' => '80'],
                'template' => '{enlace}',
                'buttons' => [
                    'enlace' => function ($url, $model, $key) {
                        return Html::a('Ver mas....', ['noticia/ver', 'id' => $model->id]);
                        // Html::a('label',['controller/action','primarykey'=>$model->id])
                        // Html::a('label', ['controller/action'])
                    },
                ],
            ],


        ],
    ]); ?>


</div>