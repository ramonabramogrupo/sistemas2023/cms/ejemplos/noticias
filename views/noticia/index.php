<?php

use app\models\Noticia;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Editando las Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Añadiendo nueva noticia', ['create'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ir a Web', ['listar'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // numero de orden
            // ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'titulo',
            //'contenido:ntext',
            'autor',
            'fechaPublicacion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Noticia $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>