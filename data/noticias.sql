﻿DROP DATABASE IF EXISTS noticiasSistemas;
CREATE DATABASE noticiasSistemas;
USE noticiasSistemas;

create TABLE noticia(
  id int AUTO_INCREMENT,
  titulo varchar(100),
  contenido text,
  autor varchar(100),
  fechaPublicacion datetime,
  PRIMARY KEY(id)
);

