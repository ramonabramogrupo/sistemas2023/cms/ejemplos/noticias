<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticia".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $contenido
 * @property string|null $autor
 * @property string|null $fechaPublicacion
 */
class Noticia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contenido'], 'string'],
            [['fechaPublicacion'], 'safe'],
            [['titulo', 'autor'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id de la noticia',
            'titulo' => 'Titulo de la noticia',
            'contenido' => 'Contenido completo',
            'autor' => 'Autor de la noticia',
            'fechaPublicacion' => 'Fecha de publicacion',
        ];
    }
}
